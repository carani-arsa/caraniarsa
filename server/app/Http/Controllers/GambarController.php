<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Gambar;

class GambarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    public function index(Request $request)
    {
        $query = Gambar::select(
            '*',
        );
        if ($request->id_properti) {
            $query->where('id_properti', '=', $request->id_properti);
        }
        $total = $query->count();
        if ($request->page || $request->per_page) {
            $page = $request->page ?? 1;
            $per_page = $request->per_page ?? 10;
            $query->skip(($page - 1) * $per_page);
            $query->limit($per_page);
        }
        $query->orderBy('created_at', 'desc');
        $result = $query->get();
        return response()->json([
            'total' => $total,
            'data' => $result,
        ]);
    }

    public function show(Request $request, $id)
    {
        $query = Gambar::select(
            '*',
        );
        $query->where('id_gambar', '=', $id);
        $result = $query->first();
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'file' => 'required|file|image',
            'id_properti' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $path = "properti/$request->id_properti";
            if (!Storage::disk('public')->has($path)) {
                Storage::disk('public')->makeDirectory($path);
            }
            $file = $request->file('file');
            $url = Storage::disk('public')->putFileAs($path, $file, $file->getClientOriginalName());
            
            $gambar = new Gambar();
            $gambar->nama = $request->nama;
            $gambar->url = $url;
            $gambar->id_properti = $request->id_properti;
            $result = $gambar->save();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'file' => 'required|file|image',
            'id_properti' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $path = "properti/$request->id_properti";
            if (!Storage::disk('public')->has($path)) {
                Storage::disk('public')->makeDirectory($path);
            }
            $file = $request->file('file');
            $url = Storage::disk('public')->putFileAs($path, $file, $file->getClientOriginalName());
            
            $gambar = Gambar::where('id_gambar', '=', $id)->first();

            // Delete old file
            $old_url = $gambar->url;
            Storage::disk('public')->delete($old_url);

            $gambar->nama = $request->nama;
            $gambar->url = $url;
            $gambar->id_properti = $request->id_properti;
            $result = $gambar->update();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $gambar = Gambar::where('id_gambar', '=', $id)->first();
            Storage::disk('public')->delete($gambar->url);
            $result = $gambar->delete();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }
}
