<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Pesan;
use App\Models\Properti;

class PesanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);
    }

    public function index(Request $request)
    {
        $query = Pesan::select(
            '*',
        );
        if ($request->id_agen) {
            $query->where('id_agen', '=', $request->id_agen);
        }
        if ($request->id_properti) {
            $query->where('id_properti', '=', $request->id_properti);
        }
        $total = $query->count();
        $query->with('agen');
        $query->with('properti');
        $query->withCount('transaksi');
        if ($request->page || $request->per_page) {
            $page = $request->page ?? 1;
            $per_page = $request->per_page ?? 10;
            $query->skip(($page - 1) * $per_page);
            $query->limit($per_page);
        }
        $query->orderBy('created_at', 'desc');
        $result = $query->get();
        return response()->json([
            'total' => $total,
            'data' => $result,
        ]);
    }

    public function show(Request $request, $id)
    {
        $query = Pesan::select(
            '*',
        );
        $query->where('id_pesan', '=', $id);
        $query->with('agen');
        $query->with('properti');
        $query->with('transaksi');
        $result = $query->first();
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_pembeli' => 'required',
            'no_telp' => 'required|max:14',
            'pesan' => 'required',
            'tanggal' => 'required|date',
            'status' => 'required',
            'id_agen' => 'required',
            'id_properti' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $pesan = new Pesan();
            $pesan->nama_pembeli = $request->nama_pembeli;
            $pesan->no_telp = $request->no_telp;
            $pesan->pesan = $request->pesan;
            $pesan->tanggal = $request->tanggal;
            $pesan->status = $request->status;
            $pesan->id_agen = $request->id_agen;
            $pesan->id_properti = $request->id_properti;
            $result = $pesan->save();
            DB::commit();
            return response()->json($pesan);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
        //     'nama_pembeli' => 'required',
        //     'no_telp' => 'required|max:14',
        //     'pesan' => 'required',
        //     'tanggal' => 'required|date',
        //     'status' => 'required',
        //     'id_agen' => 'required',
        //     'id_properti' => 'required',
        // ]);
        // if ($validator->fails()) {
        //     return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        // }

        try {
            DB::beginTransaction();
            $pesan = Pesan::where('id_pesan', '=', $id)->first();
            if ($request->nama_pembeli) $pesan->nama_pembeli = $request->nama_pembeli;
            if ($request->no_telp) $pesan->no_telp = $request->no_telp;
            if ($request->pesan) $pesan->pesan = $request->pesan;
            if ($request->tanggal) $pesan->tanggal = $request->tanggal;
            if ($request->status) $pesan->status = $request->status;
            if ($request->id_agen) $pesan->id_agen = $request->id_agen;
            if ($request->id_properti) $pesan->id_properti = $request->id_properti;
            
            // Ubah status properti -> terjual
            if ($request->status == "terjual") {
                $this->updatePropertiStatus($pesan->id_properti, "terjual");
            }

            $result = $pesan->update();
            DB::commit();
            return response()->json($pesan);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function updatePropertiStatus($id, $status)
    {
        try {
            DB::beginTransaction();
            $properti = Properti::where('id_properti', '=', $id)->first();
            $properti->status = $status;
            $properti->update();
            DB::commit();
        } catch (ValidationException  $e) {
            DB::rollBack();
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $pesan = Pesan::where('id_pesan', '=', $id)->first();
            $result = $pesan->delete();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }
}
