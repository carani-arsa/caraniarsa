<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePesanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesan', function (Blueprint $table) {
            $table->increments('id_pesan');
            $table->string('nama_pembeli');
            $table->string('no_telp', 14);
            $table->text('pesan');
            $table->dateTime('tanggal');
            $table->string('status');
            $table->unsignedInteger('id_agen');
            $table->unsignedInteger('id_properti');
            $table->timestamps();

            $table->foreign('id_agen')
                ->references('id_user')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->foreign('id_properti')
                ->references('id_properti')
                ->on('properti')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesan');
    }
}
