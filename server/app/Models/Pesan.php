<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Properti;
use App\Models\Transaksi;

class Pesan extends Model
{
    use HasFactory;
    protected $table = 'pesan';
    protected $primaryKey = 'id_pesan';
    protected $guarded = [];
    protected $hidden = [];

    public function agen()
    {
        return $this->belongsTo(User::class, 'id_agen', 'id_user');
    }

    public function properti()
    {
        return $this->belongsTo(Properti::class, 'id_properti', 'id_properti')
            ->with('pemilik');
    }

    public function transaksi()
    {
        return $this->hasOne(Transaksi::class, 'id_pesan', 'id_pesan');
    }
}
