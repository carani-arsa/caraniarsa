<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravolt\Indonesia\Models\Province;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;

class IndonesiaController extends Controller
{
    public function getProvinces(Request $request)
    {
        $query = Province::select(
            'id',
            'name',
        );
        $query->orderBy('name', 'asc');
        $result = $query->get();
        return response()->json($result);
    }

    public function getCities(Request $request)
    {
        $query = City::select(
            'id',
            'name',
            'province_id'
        );
        if ($request->province_id) {
            $query->where('province_id', '=', $request->province_id);
        }
        $query->orderBy('name', 'asc');
        $result = $query->get();
        return response()->json($result);
    }

    public function getDistricts(Request $request)
    {
        $query = District::select(
            'id',
            'name',
            'city_id'
        );
        if ($request->city_id) {
            $query->where('city_id', '=', $request->city_id);
        }
        $query->orderBy('name', 'asc');
        $result = $query->get();
        return response()->json($result);
    }
}
