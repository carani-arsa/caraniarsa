<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store', 'filter']]);
    }

    public function index(Request $request)
    {
        $query = User::select(
            '*',
        );
        $query->where('tipe', 'agen')
            ->orWhere('tipe', 'pemilik');

        if ($request->id_user) {
            $query->where('id_user', '=', $request->id_user);
        }
        $total = $query->count();
        if ($request->page || $request->per_page) {
            $page = $request->page ?? 1;
            $per_page = $request->per_page ?? 10;
            $query->skip(($page - 1) * $per_page);
            $query->limit($per_page);
        }
        $query->orderBy('id_user', 'asc');
        $result = $query->get();
        return response()->json([
            'total' => $total,
            'data' => $result,
        ]);
    }

    public function filter(Request $request, $tipe)
    {
        $query = User::select(
            '*',
        );
        $query->where('tipe', '=', $tipe);

        if($tipe == 'agen'){
            $query->orderBy('rata_rata_penjualan', $request->sortType ?? "asc");
        }

        $total = $query->count();
        if ($request->page || $request->per_page) {
            $page = $request->page ?? 1;
            $per_page = $request->per_page ?? 10;
            $query->skip(($page - 1) * $per_page);
            $query->limit($per_page);
        }
        $query->orderBy('id_user', 'asc');
        $result = $query->get();
        return response()->json([
            'total' => $total,
            'data' => $result,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tipe' => 'required',
            'nama' => 'required|max:50',
            'jenis_kelamin' => 'required',
            'no_telp' => 'required|max:14',
            'alamat' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $user = new User();
            $user->tipe = $request->tipe;
            $user->nama = $request->nama;
            $user->jenis_kelamin = $request->jenis_kelamin;
            $user->no_telp = $request->no_telp;
            $user->alamat = $request->alamat;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $result = $user->save();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $query = User::select(
            '*',
        );
        $query->where('id_user', '=', $id);
        $result = $query->first();
        return response()->json($result);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'tipe' => 'required',
            'nama' => 'required|max:50',
            'jenis_kelamin' => 'required',
            'no_telp' => 'required|max:14',
            'alamat' => 'required',
            'email' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $user = User::where('id_user', '=', $id)->first();
            $user->tipe = $request->tipe;
            $user->nama = $request->nama;
            $user->jenis_kelamin = $request->jenis_kelamin;
            $user->no_telp = $request->no_telp;
            $user->alamat = $request->alamat;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $result = $user->update();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tipe' => 'required',
            'nama' => 'required|max:50',
            'jenis_kelamin' => 'required',
            'no_telp' => 'required|max:14',
            'alamat' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $id = Auth::user()->id_user;
            $user = User::where('id_user','=',$id)->first();
            $user->tipe = $request->tipe;
            $user->nama = $request->nama;
            $user->jenis_kelamin = $request->jenis_kelamin;
            $user->no_telp = $request->no_telp;
            $user->alamat = $request->alamat;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $result = $user->update();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function profileDetail(Request $request)
    {
        $query = User::select(
            '*',
        );
        $id = Auth::user()->id_user;
        $query->where('id_user', '=', $id);
        $result = $query->first();
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $pesan = User::where('id_user', '=', $id)->first();
            $result = $pesan->delete();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }
}
