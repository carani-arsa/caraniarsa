<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        DB::table('user')->insert([
            'tipe' => 'admin',
            'nama' => 'Admin Carani Arsa',
            'jenis_kelamin' => 'P',
            'no_telp' => '081001002003',
            'alamat' => 'Jl. Anggrek Gg. Mawar No. 7',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
        // Pemilik
        DB::table('user')->insert([
            'tipe' => 'pemilik',
            'nama' => 'Pemilik',
            'jenis_kelamin' => 'P',
            'no_telp' => '081001002003',
            'alamat' => 'Jl. Anggrek Gg. Mawar No. 7',
            'email' => 'pemilik@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
        // Agen
        DB::table('user')->insert([
            'tipe' => 'agen',
            'nama' => 'Agen',
            'jenis_kelamin' => 'P',
            'no_telp' => '081001002003',
            'alamat' => 'Jl. Anggrek Gg. Mawar No. 7',
            'email' => 'agen@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
    }
}
