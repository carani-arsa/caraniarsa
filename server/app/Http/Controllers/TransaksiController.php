<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Transaksi;
use App\Models\Pesan;
use App\Models\User;

class TransaksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $query = Transaksi::select(
            '*',
        );
        if ($request->id_pesan) {
            $query->where('id_pesan', '=', $request->id_pesan);
        }
        $total = $query->count();
        if ($request->page || $request->per_page) {
            $page = $request->page ?? 1;
            $per_page = $request->per_page ?? 10;
            $query->skip(($page - 1) * $per_page);
            $query->limit($per_page);
        }
        $query->orderBy('created_at', 'desc');
        $result = $query->get();
        return response()->json([
            'total' => $total,
            'data' => $result,
        ]);
    }

    public function show(Request $request, $id)
    {
        $query = Transaksi::select(
            '*',
        );
        $query->where('id_transaksi', '=', $id);
        $result = $query->first();
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nominal' => 'required|numeric',
            'tanggal' => 'required|date',
            'id_pesan' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $transaksi = new Transaksi();
            $transaksi->nominal = $request->nominal;
            $transaksi->tanggal = $request->tanggal;
            $transaksi->id_pesan = $request->id_pesan;
            $this->updateRataRata($transaksi);
            $result = $transaksi->save();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function updateRataRata($transaksi)
    {
        try {
            DB::beginTransaction();

            $pesan = Pesan::where('id_pesan', '=', $transaksi->id_pesan)->first();
            $user = User::where('id_user', '=', $pesan->id_agen)->first();
            $user->rata_rata_penjualan = ($user->rata_rata_penjualan + $transaksi->nominal) / 2;
            $result = $user->update();

            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nominal' => 'required|numeric',
            'tanggal' => 'required|date',
            'id_pesan' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $transaksi = Transaksi::where('id_transaksi', '=', $id)->first();
            $transaksi->nominal = $request->nominal;
            $transaksi->tanggal = $request->tanggal;
            $transaksi->id_pesan = $request->id_pesan;
            $result = $transaksi->update();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $transaksi = Transaksi::where('id_transaksi', '=', $id)->first();
            $result = $transaksi->delete();
            DB::commit();
            return response()->json($result);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }
}
