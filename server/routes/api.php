<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\IndonesiaController;
use App\Http\Controllers\PropertiController;
use App\Http\Controllers\FasilitasController;
use App\Http\Controllers\GambarController;
use App\Http\Controllers\PesanController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::get('me', [AuthController::class, 'me']);
});

Route::group(['middleware' => 'api','prefix' => 'dashboard'], function ($router) {
    Route::get('total', [DashboardController::class, 'getTotal']);
});

Route::group(['middleware' => 'api','prefix' => 'indonesia'], function ($router) {
    Route::get('provinsi', [IndonesiaController::class, 'getProvinces']);
    Route::get('kota', [IndonesiaController::class, 'getCities']);
    Route::get('kecamatan', [IndonesiaController::class, 'getDistricts']);
});

Route::group(['middleware' => 'api','prefix' => 'properti'], function ($router) {
    Route::get('', [PropertiController::class, 'index']);
    Route::get('{id}', [PropertiController::class, 'show']);
    Route::post('', [PropertiController::class, 'store']);
    Route::put('{id}', [PropertiController::class, 'update']);
    Route::delete('{id}', [PropertiController::class, 'destroy']);
});

Route::group(['middleware' => 'api','prefix' => 'gambar'], function ($router) {
    Route::get('', [GambarController::class, 'index']);
    Route::get('{id}', [GambarController::class, 'show']);
    Route::post('', [GambarController::class, 'store']);
    Route::post('{id}', [GambarController::class, 'update']);
    Route::delete('{id}', [GambarController::class, 'destroy']);
});

Route::group(['middleware' => 'api','prefix' => 'pesan'], function ($router) {
    Route::get('', [PesanController::class, 'index']);
    Route::get('{id}', [PesanController::class, 'show']);
    Route::post('', [PesanController::class, 'store']);
    Route::put('{id}', [PesanController::class, 'update']);
    Route::delete('{id}', [PesanController::class, 'destroy']);
});

Route::group(['middleware' => 'api','prefix' => 'user'], function ($router) {
    Route::get('', [UserController::class, 'index']);
    Route::get('profile', [UserController::class, 'profileDetail']);
    Route::get('filter/{tipe}', [UserController::class, 'filter']);
    Route::get('{id}', [UserController::class, 'show']);
    Route::post('', [UserController::class, 'store']);
    Route::put('profile', [UserController::class, 'updateProfile']);
    Route::put('{id}', [UserController::class, 'update']);
    Route::delete('{id}', [UserController::class, 'destroy']);
});

Route::group(['middleware' => 'api','prefix' => 'transaksi'], function ($router) {
    Route::get('', [TransaksiController::class, 'index']);
    Route::get('{id}', [TransaksiController::class, 'show']);
    Route::post('', [TransaksiController::class, 'store']);
    Route::put('{id}', [TransaksiController::class, 'update']);
    Route::delete('{id}', [TransaksiController::class, 'destroy']);
});
