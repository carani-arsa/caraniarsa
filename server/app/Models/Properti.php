<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Indonesia\Models\Province;
use Laravolt\Indonesia\Models\City;
use Laravolt\Indonesia\Models\District;
use App\Models\User;
use App\Models\Gambar;
use App\Models\Pesan;

class Properti extends Model
{
    use HasFactory;
    protected $table = 'properti';
    protected $primaryKey = 'id_properti';
    protected $guarded = [];
    protected $hidden = [];

    public function agen()
    {
        return $this->belongsTo(User::class, 'id_agen', 'id_user');
    }

    public function pemilik()
    {
        return $this->belongsTo(User::class, 'id_pemilik', 'id_user');
    }

    public function gambar()
    {
        return $this->hasMany(Gambar::class, 'id_properti', 'id_properti');
    }

    public function pesanTerjual()
    {
        return $this->hasOne(Pesan::class, 'id_properti', 'id_properti')->where('pesan.status', '=', 'terjual');
    }

    public function kecamatan()
    {
        return $this->belongsTo(District::class, 'id_kecamatan', 'id');
    }

    public function kota()
    {
        return $this->belongsTo(City::class, 'id_kota', 'id');
    }

    public function provinsi()
    {
        return $this->belongsTo(Province::class, 'id_provinsi', 'id');
    }
}
