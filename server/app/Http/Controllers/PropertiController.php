<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Properti;

class PropertiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    public function index(Request $request)
    {
        $query = Properti::select(
            '*',
        );
        if ($request->tahun) {
            $query->where('tahun', '=', $request->tahun);
        }
        if ($request->id_pemilik) {
            $query->where('id_pemilik', '=', $request->id_pemilik);
        }
        if ($request->id_agen) {
            $query->where('id_agen', '=', $request->id_agen);
        }
        if ($request->id_kecamatan) {
            $query->where('id_kecamatan', '=', $request->id_kecamatan);
        }
        if ($request->id_kota) {
            $query->where('id_kota', '=', $request->id_kota);
        }
        if ($request->id_provinsi) {
            $query->where('id_provinsi', '=', $request->id_provinsi);
        }
        if ($request->harga_mulai) {
            $query->where('harga_jual', '>=', $request->harga_mulai);
        }
        if ($request->harga_sampai) {
            $query->where('harga_jual', '<=', $request->harga_sampai);
        }
        if ($request->kamar) {
            $query->where('kamar', '=', $request->kamar);
        }
        if ($request->toilet) {
            $query->where('toilet', '=', $request->toilet);
        }
        if ($request->kolam_renang) {
            $query->where('kolam_renang', '=', $request->kolam_renang);
        }
        if ($request->lantai) {
            $query->where('lantai', '=', $request->lantai);
        }
        if ($request->status) {
            $query->where('status', '=', $request->status);
        }
        if ($request->nama) {
            $query->where('nama', 'LIKE', "%$request->nama%");
        }
        $total = $query->count();
        if ($request->page || $request->per_page) {
            $page = $request->page ?? 1;
            $per_page = $request->per_page ?? 10;
            $query->skip(($page - 1) * $per_page);
            $query->limit($per_page);
        }
        $query->with('agen');
        $query->with('pemilik');
        $query->with('gambar');
        $query->with('pesanTerjual');
        $query->orderBy('created_at', 'desc');
        $result = $query->get();
        return response()->json([
            'total' => $total,
            'data' => $result,
        ]);
    }

    public function show(Request $request, $id)
    {
        $query = Properti::select(
            '*',
        );
        $query->where('id_properti', '=', $id);
        $query->with('agen');
        $query->with('pemilik');
        $query->with('gambar');
        $query->with('kecamatan');
        $query->with('kota');
        $query->with('provinsi');
        $result = $query->first();
        return response()->json($result);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'deskripsi' => 'required',
            'tipe_properti' => 'required',
            'tahun' => 'required',
            'harga' => 'required',
            'harga_jual' => 'required',
            'komisi' => 'required',
            'sertifikat' => 'nullable',
            'luas_tanah' => 'required|numeric',
            'luas_bangunan' => 'required|numeric',
            'listrik' => 'required|numeric',
            'kamar' => 'required|numeric',
            'toilet' => 'required|numeric',
            'kolam_renang' => 'required|numeric',
            'lantai' => 'required|numeric',
            'status' => 'required',
            'alamat' => 'required',
            'id_pemilik' => 'required',
            'id_kecamatan' => 'required',
            'id_kota' => 'required',
            'id_provinsi' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        try {
            DB::beginTransaction();
            $properti = new Properti();
            $properti->nama = $request->nama;
            $properti->deskripsi = $request->deskripsi;
            $properti->tipe_properti = $request->tipe_properti;
            $properti->tahun = $request->tahun;
            $properti->harga = $request->harga;
            $properti->harga_jual = $request->harga_jual;
            $properti->komisi = $request->komisi;
            $properti->sertifikat = $request->sertifikat;
            $properti->luas_tanah = $request->luas_tanah;
            $properti->luas_bangunan = $request->luas_bangunan;
            $properti->listrik = $request->listrik;
            $properti->kamar = $request->kamar;
            $properti->toilet = $request->toilet;
            $properti->kolam_renang = $request->kolam_renang;
            $properti->lantai = $request->lantai;
            $properti->status = $request->status;
            $properti->alamat = $request->alamat;
            $properti->id_pemilik = $request->id_pemilik;
            $properti->id_kecamatan = $request->id_kecamatan;
            $properti->id_kota = $request->id_kota;
            $properti->id_provinsi = $request->id_provinsi;
            $properti->save();
            DB::commit();
            return response()->json($properti);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
        //     'nama' => 'required',
        //     'deskripsi' => 'required',
        //     'tipe_properti' => 'required',
        //     'tahun' => 'required',
        //     'harga' => 'required',
        //     'sertifikat' => 'nullable',
        //     'luas_tanah' => 'required|numeric',
        //     'luas_bangunan' => 'required|numeric',
        //     'listrik' => 'required|numeric',
        //     'kamar' => 'required|numeric',
        //     'toilet' => 'required|numeric',
        //     'kolam_renang' => 'required|numeric',
        //     'lantai' => 'required|numeric',
        //     'status' => 'required',
        //     'id_pemilik' => 'required',
        //     'id_kecamatan' => 'required',
        //     'id_kota' => 'required',
        //     'id_provinsi' => 'required',
        // ]);
        // if ($validator->fails()) {
        //     return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        // }

        try {
            DB::beginTransaction();
            $properti = Properti::where('id_properti', '=', $id)->first();
            if ($request->nama) $properti->nama = $request->nama;
            if ($request->deskripsi) $properti->deskripsi = $request->deskripsi;
            if ($request->tipe_properti) $properti->tipe_properti = $request->tipe_properti;
            if ($request->tahun) $properti->tahun = $request->tahun;
            if ($request->harga) $properti->harga = $request->harga;
            if ($request->harga_jual) $properti->harga_jual = $request->harga_jual;
            if ($request->komisi) $properti->komisi = $request->komisi;
            if ($request->sertifikat) $properti->sertifikat = $request->sertifikat;
            if ($request->luas_tanah) $properti->luas_tanah = $request->luas_tanah;
            if ($request->luas_bangunan) $properti->luas_bangunan = $request->luas_bangunan;
            if ($request->listrik) $properti->listrik = $request->listrik;
            if ($request->kamar) $properti->kamar = $request->kamar;
            if ($request->toilet) $properti->toilet = $request->toilet;
            if ($request->kolam_renang) $properti->kolam_renang = $request->kolam_renang;
            if ($request->lantai) $properti->lantai = $request->lantai;
            if ($request->status) $properti->status = $request->status;
            if ($request->alamat) $properti->alamat = $request->alamat;
            if ($request->deskripsi_penolakan) $properti->deskripsi_penolakan = $request->deskripsi_penolakan;
            if ($request->id_agen) $properti->id_agen = $request->id_agen;
            if ($request->id_pemilik) $properti->id_pemilik = $request->id_pemilik;
            if ($request->id_kecamatan) $properti->id_kecamatan = $request->id_kecamatan;
            if ($request->id_kota) $properti->id_kota = $request->id_kota;
            if ($request->id_provinsi) $properti->id_provinsi = $request->id_provinsi;

            // Jika status diterima
            if ($request->status == "diterima") {
                $properti->deskripsi_penolakan = null;
            }
            $properti->update();
            DB::commit();
            return response()->json($properti);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $properti = Properti::where('id_properti', '=', $id)->first();
            $properti->delete();
            DB::commit();
            return response()->json($properti);
        } catch (ValidationException  $e) {
            DB::rollBack();
            return response()->json($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }
}
