<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Properti;
use App\Models\Transaksi;

class LaporanController extends Controller
{
    public function laporanProperti(Request $request) {
        $query = Properti::select("properti.*");
        $query->join("pesan", "pesan.id_properti", "properti.id_properti");
        $query->join("transaksi", "pesan.id_pesan", "transaksi.id_pesan");
        $query->where("properti.status", "terjual");
        $query->where("pesan.status", "terjual");
        $query->whereYear("transaksi.tanggal", $request->tahun);

        // Sum nominal transaksi
        $total_transaksi = $query->sum("transaksi.nominal");

        $query->with("agen");
        $query->with("pemilik");
        $query->with("pesanTerjual");
        $data = $query->get();

        return View::make('laporan.laporan_properti', [
            'no' => 1,
            'tahun' => $request->tahun,
            'data' => $data,
            'total_transaksi' => $total_transaksi,
        ]);
    }
}
