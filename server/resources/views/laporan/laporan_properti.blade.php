<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Laporan Properti</title>
  <style>
    table {
      width: 100%;
      border-collapse: collapse;
    }
    td {
      padding: 4px;
    }
    .title {
      font-size: 30px;
      font-weight: bold;
      margin-bottom: 5px;
    }
    .subtitle {
      font-size: 20px;
      margin-bottom: 20px;
    }
    .text-bold {
      font-weight: bold;
    }
    .text-right {
      text-align: right;
    }
    .text-center {
      text-align: center;
    }
  </style>
</head>
<body>
  <div class="title text-center">Laporan Properti Terjual</div>
  <div class="subtitle text-center"> Tahun {{ $tahun }}</div>
  <table border="1">
    <tr>
      <th class="text-center" style="width: 50px">No</th>
      <th>Nama Properti</th>
      <th class="text-right">Harga Properti (Rp)</th>
      <th class="text-right">Harga Jual (Rp)</th>
      <th>Agen</th>
      <th>Nama Pembeli</th>
      <th>No. Telp Pembeli</th>
      <th class="text-right">Nominal Transaksi (Rp)</th>
    </tr>
    @foreach ($data as $d)
    <tr>
      <td class="text-center">{{ $no++ }}</td>
      <td>{{ $d->nama }}</td>
      <td class="text-right">{{ number_format($d->harga, 0, ',', '.') }}</td>
      <td class="text-right">{{ number_format($d->harga_jual, 0, ',', '.') }}</td>
      <td>{{ $d->agen->nama }}</td>
      <td>{{ $d->pesanTerjual ? $d->pesanTerjual->nama_pembeli : "-" }}</td>
      <td>{{ $d->pesanTerjual ? $d->pesanTerjual->no_telp : "-" }}</td>
      <td class="text-right">{{ $d->pesanTerjual && $d->pesanTerjual->transaksi ? number_format($d->pesanTerjual->transaksi->nominal, 0, ',', '.') : "-" }}</td>
    </tr>
    @endforeach
    <tr>
      <td colspan="7" class="text-center text-bold">Total</td>
      <td class="text-right text-bold">{{  number_format($total_transaksi, 0, ',', '.') }}</td>
    </tr>
  </table>
</body>
</html>