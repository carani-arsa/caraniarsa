Memerlukan
1. **Composer**, bisa di download pada link berikut: <br /> 
> https://getcomposer.org/download/
2. **XAMPP**, dengan php versi >= 7.3.0, bisa di download pada link berikut <br />
> https://www.apachefriends.org/download.html

Cara install:
1. Membuat database mysql dengan nama **caraniarsa**
2. Buka terminal/cmd lalu clone repoitory ini pada folder **C:&bsol;&bsol;xampp&bsol;htdocs** dengan perintah: <br />
> `git clone https://gitlab.com/carani-arsa/caraniarsa.git` <br />
3. Kemudian masuk ke folder project dengan mengetik perintah pada terminal/cmd: <br />
> `cd caraniarsa`
4. Kemudian masuk ke folder server dengan mengetik perintah pada terminal/cmd: <br />
> `cd server`
5. Jalankan perintah berikut secara berurutan: <br />
> `composer install` <br />

> `php artisan storage:link` <br />

> `php artisan migrate --seed` <br />

> `php artisan laravolt:indonesia:seed` <br />

> `chmod -R 755 storage bootstrap/cache` <br />
6. Buka browser dan ketik pada url pencarian untuk membuat website: <br />
> http://localhost/caraniarsa

Berikut user yang terdaftar pada DB :
1. Admin:
    - Username = admin@gmail.com
    - Password = 12345678

2. Agent:
    - Username = agen@gmail.com
    - Password = 12345678

3. Pemilik:
    - Username = pemilik@gmail.com
    - Password = 12345678

Berikut penjelasan mengenai folder yang ada pada program ini :

1. Folder dashboard, berisi halaman untuk mengelola crud seperti page admin, agen, pemilik, properti, laporan, pesan, home dashboard. Folder dashboard ini khusus untuk mengelola crud atau dapat dikatakan folder yang berisi semua logika dan proses untuk admin. Pada folder dashboard ini terdapat folder admin, nah di dalam foder admin ini terdapat logic yang berisi crud untuk admin. Baik halaman index, halaman form untuk mengisi data admin.

2. Folder auth, berisi mengenai fungsi logic login dan register untuk login sebagai admin, agen, pemilik serta berisi logic untuk register pemilik baru.
3. Folder app, berisi mengenai tampilan untuk user seperti homepage dan detail properti serta filter property 
4. Folder server, berisi file-file untuk mengelola proses website dengan database
5. Folder image, berisi mengenai images atau foto-foto yang disimpan pada direktori local. Semua images akan dikumpulkan difolder ini
6. Index.html, merupakan file utama yang sudah di compile dan dijalankan oleh browser.
7. Readme.md, berisi documentation bagaimana cara menjalankan program ini


