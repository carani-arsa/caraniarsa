<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properti', function (Blueprint $table) {
            $table->increments('id_properti');
            $table->string('nama');
            $table->text('deskripsi');
            $table->string('tipe_properti');
            $table->string('tahun', 4);
            $table->double('harga');
            $table->string('sertifikat')->nullable();
            $table->double('luas_tanah');
            $table->double('luas_bangunan');
            $table->double('listrik');
            $table->tinyInteger('kamar');
            $table->tinyInteger('toilet');
            $table->tinyInteger('kolam_renang');
            $table->tinyInteger('lantai');
            $table->string('status');
            $table->unsignedInteger('id_agen')->nullable();
            $table->unsignedInteger('id_pemilik');
            $table->char('id_kecamatan', 7);
            $table->char('id_kota', 4);
            $table->char('id_provinsi', 2);
            $table->timestamps();

            $table->foreign('id_agen')
                ->references('id_user')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('id_pemilik')
                ->references('id_user')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('id_kecamatan')
                ->references('id')
                ->on(config('laravolt.indonesia.table_prefix').'districts')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->foreign('id_kota')
                ->references('id')
                ->on(config('laravolt.indonesia.table_prefix').'cities')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->foreign('id_provinsi')
                ->references('id')
                ->on(config('laravolt.indonesia.table_prefix').'provinces')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properti');
    }
}
